<?php
/* @var $this SortController */
/* @var $data CArrayDataProvider */

$this->pageTitle=Yii::app()->name . ' - 2.) feladat';
?>
<h1>2. feladat</h1>
<p><b>Nettó bérek rendezése</b></p>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $data,
    'columns' => array(
        array(
            'name' => 'Név',
            'type' => 'raw',
            'value' => 'CHtml::encode($data[\'nev\'])'
        ),
        array(
            'name' => 'Nettó fizetés',
            'type' => 'raw',
            'value' => 'CHtml::encode($data[\'netto\'])',
        ),
    ),
));
?>