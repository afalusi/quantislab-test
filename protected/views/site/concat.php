<?php
/* @var $this SiteController */
/* @var $even String */
/* @var $odd String */
/* @var $leftOver String */

$this->pageTitle=Yii::app()->name . ' - 4.) feladat';
?>

<h1>4.) Feladat</h1>

<h2>Tömb páros és páratlan elemeinek összefűzése</h2>

<table>
    <tr>
        <th>Páros elemek:</th>
        <td><?php echo CHtml::encode($even); ?></td>
    </tr>
    <tr>
        <th>Páratlan elemek:</th>
        <td><?php echo CHtml::encode($odd); ?></td>
    </tr>
<?php if ($leftOver): ?>
    <tr>
        <th>Magányos elem:</th>
        <td><?php echo CHtml::encode($leftOver); ?></td>
    </tr>
<?php endif; ?>
</table>