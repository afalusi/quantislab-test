<?php
/* @var $this SiteController */
/* @var $data Array */

$this->pageTitle=Yii::app()->name . ' - 1.) feladat';
?>

<h1>1.) Feladat</h1>

<p><b>Bruttó bér járulékai</b></p>

<table>
<?php foreach ($data as $header => $value): ?>
    <tr>
        <th><?php echo CHtml::encode($header); ?></th>
        <td><?php echo CHtml::encode($value); ?></td>
    </tr>
<?php endforeach; ?>
</table>