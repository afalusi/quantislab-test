<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Időjárás';
?>

<h1>5.) - 6.) feladat</h1>

<p><b>Időjárás lekérdezése Yahoo szerverről, grafikonnal való ábrázolása</b></p>

<?php if(Yii::app()->user->hasFlash('WeatherNoResult')): ?>

<div class="flash-error">
	<?php echo Yii::app()->user->getFlash('WeatherNoResult'); ?>
</div>

<?php endif; ?>


<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'weather-form',
        'action' => CHtml::normalizeUrl(array('site/weather')),
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'location'); ?>
        <?php echo $form->textField($model,'location'); ?>
        <?php echo $form->error($model,'location'); ?>
    </div>
    <div class="row buttons">
        <?php echo CHtml::submitButton('Keresés'); ?>
    </div>
<?php $this->endWidget(); ?>
<?php if ($model->getPossibilities()): ?>
    <?php $this->widget('zii.widgets.CMenu',array(
        'items'=>$model->getPossibilities()
    )); ?>
<?php endif; ?>
<?php if ($model->getWeather()): ?>
    <h3><?php echo $model->getWeather()->channel->item->title; ?></h3>
    <?php echo $model->getWeather()->channel->item->description; ?>

    <script src="http://code.highcharts.com/highcharts.js"></script>
    <script src="http://code.highcharts.com/modules/exporting.js"></script>

    <div id="forecast-container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

<script type="text/javascript">

$(function () {
    $('#forecast-container').highcharts({
        chart: {
            type: 'spline'
        },
        title: {
            text: 'Forecast'
        },
        xAxis: {
            categories: <?php echo json_encode($model->getForecastData('days')) ?>
        },
        yAxis: {
            title: {
                text: 'Temperature'
            },
            labels: {
                formatter: function () {
                    return this.value + '°';
                }
            }
        },
        tooltip: {
            crosshairs: true,
            shared: true
        },
        plotOptions: {
            spline: {
                marker: {
                    radius: 4,
                    lineColor: '#666666',
                    lineWidth: 1
                }
            }
        },
        series: [{
            name: 'Maximum',
            marker: {
                symbol: 'square'
            },
            data: <?php echo json_encode($model->getForecastData('maxs')) ?>

        }, {
            name: 'Minimum',
            marker: {
                symbol: 'diamond'
            },
            data: <?php echo json_encode($model->getForecastData('mins')) ?>
        }]
    });
});</script>

<?php endif; ?>
</div><!-- form -->
