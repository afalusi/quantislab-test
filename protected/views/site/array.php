<?php
/* @var $this SiteController */
/* @var $data CArrayDataProvider */

$this->pageTitle=Yii::app()->name . ' - 3.) feladat';
?>
<h1>3.) feladat</h1>
<p><b>Tömbműveletek négy megadott tömbön</b></p>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $data,
    'columns' => array(
        array(
            'name' => 'Alap',
            'type' => 'raw',
            'value' => 'CHtml::encode($data[\'base\'])'
        ),
        array(
            'name' => 'Rendezett',
            'type' => 'raw',
            'value' => 'CHtml::encode($data[\'sorted\'])',
        ),
        array(
            'name' => 'Összeg',
            'type' => 'raw',
            'value' => 'CHtml::encode($data[\'sum\'])',
        ),
        array(
            'name' => 'Átlag',
            'type' => 'raw',
            'value' => 'CHtml::encode($data[\'avg\'])',
        ),
    ),
));
?>