<?php
/**
 * WeatherForm class.
 * WeatherForm is the data structure for keeping
 * user weather form data. It is used by the 'weather' action of 'SiteController'.
 */
class WeatherForm extends CFormModel
{
    public $location;
    /**
     * Időjárás válasz a yahootól
     */
    protected $weather = array();
    /**
     * Választható helyek listája, több hasonló nevű hely esetén
     */
    protected $possibilities = array();
    /**
     * Előrejelzési adatok
     */
    protected $forecastData = array();

    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules()
    {
        return array(
            // username and password are required
            array('location', 'required'),
        );
    }

    /**
     * Lekérdezi a keresett helyszínre hasonlító nevű helyeket a Yahoo szerveréről
     */
    public function curlPlaces()
    {
        $BASE_URL = "http://query.yahooapis.com/v1/public/yql";

        $yql_query = "select woeid, name, country from geo.places where text='".$this->location."' and placeTypeName = 'Town'";
        $yql_query_url = $BASE_URL . "?q=" . urlencode($yql_query) . "&format=json";

        // cURL hívás
        $session = curl_init($yql_query_url);
        curl_setopt($session, CURLOPT_RETURNTRANSFER,true);
        $json = curl_exec($session);

        // Válasz visszafejtése
        $response =  json_decode($json);

        if ($response->query->results === null) { // Nincs találat
            Yii::app()->user->setFlash('WeatherNoResult', 'Nincs a keresésnek megfelelő hely.');
        } elseif (count($response->query->results->place) > 1) { // Több találat
            $results = $response->query->results->place;
            foreach ($results as $row) {
                $this->possibilities[] = array(
                    'label' => $row->name.', '.$row->country->content,
                    'url' => CHtml::normalizeUrl(array('site/weather', 'woeid' => $row->woeid)),
                );
            }
        } else { // Egy találat
            $this->curlWeather($response->query->results->place->woeid);
        }
    }

    /**
     * Lekérdezi az adott place id alapján a helyi időjárást a yahootól
     * 
     * @param int $woeid
     */
    public function curlWeather($woeid) {
        $yql_query_url = 'http://weather.yahooapis.com/forecastrss?w='.$woeid.'&u=c';

        // cURL hívás
        $session = curl_init($yql_query_url);
        curl_setopt($session, CURLOPT_RETURNTRANSFER,true);
        $response = curl_exec($session);

        // Válasz visszafejtése
        $this->weather = simplexml_load_string($response);

        foreach ($this->weather->xpath('//yweather:forecast') as $forecast) {
            $this->forecastData['days'][] = (string)$forecast['day'];
            $this->forecastData['mins'][] = (int)$forecast['low'];
            $this->forecastData['maxs'][] = (int)$forecast['high'];
        }
    }

    /**
     * Encapsulation eljárás weather propertyhez
     * 
     * @return array
     */
    public function getWeather() {
        return $this->weather;
    }

    /**
     * Encapsulation eljárás possibilities propertyhez
     * 
     * @return array
     */
    public function getPossibilities() {
        return $this->possibilities;
    }

    /**
     * Encapsulation eljárás forecastData propertyhez
     * 
     * @return array
     */
    public function getForecastData($type) {
        return isset($this->forecastData[$type]) ? $this->forecastData[$type] : array();
    }
}
