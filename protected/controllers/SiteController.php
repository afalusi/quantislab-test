<?php

class SiteController extends Controller
{
        /**
         * Controller default action, bruttó bér visszafejtése (1. feladat)
         */
	public function actionIndex()
	{
            $this->render('salary',array('data'=>$this->salaryCalculator('Falusi András', 458000)));
	}
        /**
         * Visszaadja egy asszociatív tömbben a bruttó fizetés járulékait (1. feladat)
         * 
         * @param string $name
         * @param int $brutto
         * @return array
         */
        protected function salaryCalculator($name, $brutto) {
            if (!is_numeric($brutto)) {$brutto = 0;}
            $result = array(
                $name, // Név
                $brutto // Bruttó
            );
            array_push(
                $result,
                $brutto * 0.16, // SZJA
                $brutto * 0.1, // Nyugdíj járulék
                $brutto * 0.085, // Egészségügyi hozzájárulás
                $brutto * (1 - (0.16 + 0.1 + 0.085)) // Nettó
            );
            $headers = array('Név', 'Bruttó', 'SZJA', 'Nyugdíj járulék', 'Egészségügyi hozzájárulás', 'Nettó bér');
            return array_combine($headers, $result);
        }

        /**
         * Controller action Array (3. feladat)
         */
        public function actionArray()
	{
            $array1 = array(97, 154, 66, 75, 122, 13254, 11, 0, 14);
            $array2 = array("A" => "Audi", "Z" => "BMW z4", "F" => "LaFerrari", "H" => "Hummer h2", "C" => "Corvette", "VW" => "VolksWagen Scirocco");
            $array3 = array("kutya", "alma", "macska", "hangya", "pocak", "nadrag", "golyostoll", "cekla", "korte", "angyal", "aaa2545", 14, 26, 48);
            $array4 = array (1.25, 63.44, 24.89, 6565.233331, 454, 1099, 6547);
            $dataProvider = new CArrayDataProvider(
                $this->processArrays($array1, $array2, $array3, $array4),
                array(
                    'keyField' => 'base',
                    'sort'=>array(
                        'attributes'=>array('base', 'sorted', 'sum', 'avg'),
                        'defaultOrder'=>array('base' => false),
                ),
            ));
            $this->render('array',array('data'=>$dataProvider));
	}

        /**
         * Tömbműveleteket végző függvény (3. feladat)
         * 
         * @param array $array1
         * @param array $array2
         * @param array $array3
         * @param array $array4
         * @return array
         */
        protected function processArrays($array1, $array2, $array3, $array4) {
            $arrays = array();
            for ($i = 1; $i <= 4; $i++) {
                $arrayName = 'array'.$i;
                $currentArray = $$arrayName;
                if (!is_array($currentArray)) {
                    continue;
                }
                $subArray = array();
                $subArray['base'] = implode(', ', $currentArray);
                asort($currentArray);
                $subArray['sorted'] = implode(', ', $currentArray);
                $subArray['sum'] = (array_sum($currentArray) > 0) ? array_sum($currentArray) : 'N/A';
                $subArray['avg'] = ($this->arrayLookup($currentArray)) ? array_sum($currentArray) / count($currentArray) : 'N/A';
                $arrays[] = $subArray;
            }
            return $arrays;
        }
        /**
         * Segédfüggvény mely egy tömbről megállapítja, hogy csak szám van-e benne vagy sem. (3. feladat)
         * 
         * @param array $array
         * @return boolean
         */
        protected function arrayLookup($array) {
            if (!is_array($array)) {return false;}
            foreach ($array as $item) {
                if (!is_numeric($item))
                    return false;
            }
            return true;
        }

        /**
         * Controller action Concat (4. feladat)
         */
        public function actionConcat() {
            $array = array('lajos', 'kiraly', 'pista', 'aladar', 'jozsef');
            $leftOver = '';
            $odd = array();
            $even = array();
            $both = array(&$even, &$odd);
            array_walk($array, function($v, $k) use ($both) { $both[$k % 2][] = $v; });
            if (count($even) != count($odd)) {
                $leftOver = '['.array_pop($even).']';
            }
            $even = implode('_', $even);
            $odd = implode('_', $odd);
            $this->render(
                'concat',
                array(
                    'even' => $even,
                    'odd' => $odd,
                    'leftOver' => $leftOver
                )
            );
        }

        /**
	 * Controller action Weather (5. és 6. feladat)
	 */
	public function actionWeather()
	{
            $model=new WeatherForm;
            $woeid = Yii::app()->request->getParam('woeid');
            if ($woeid && preg_match('|^\d+$|', $woeid)) {
                $model->curlWeather($woeid);
            } else {
                // if it is ajax validation request
                if(isset($_POST['ajax']) && $_POST['ajax']==='weather-form')
                {
                        echo CActiveForm::validate($model);
                        Yii::app()->end();
                }

                // collect user input data
                if(isset($_POST['WeatherForm']))
                {
                        $model->attributes=$_POST['WeatherForm'];
                        // validate user input and redirect to the previous page if valid
                        $model->curlPlaces();
                }
            }
            $this->render('weather',array('model'=>$model));
	}

        /**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
}