<?php

class SortController extends Controller
{
	/**
	 * Controller default action, csak azért raktam külön controllerbe, mert a feladat kimondja. (2. feladat)
	 */
	public function actionIndex()
	{
            $array = array(
                'Kiss Janos' => 170000,
                'Nagy Jozsef' => 198500,
                'Szegeny Bela' => 2500000,
                'Magnas Miska' => 750000,
                'Gipsz Jakab' => 458010
            );
            $this->convertData($array);
            $dataProvider = new CArrayDataProvider(
                $array,
                array(
                    'keyField' => 'nev',
                    'sort'=>array(
                        'attributes'=>array('nev', 'netto'),
                        'defaultOrder'=>array('netto' => true),
                ),
            ));
            $this->render('salarysort',array('data'=>$dataProvider));
	}

        /**
         * Átalakító függvény ami a CArrayDataProvider osztály számára értelmezhető formába hozza a tömböt
         * 
         * @param array $array
         * @return array
         */
        protected function convertData(&$array) {
            if (!is_array($array))
            {
                return array();
            } else {
                array_walk($array, array('SortController', 'convertRow'));
                $array = array_values($array);
            }
        }
        /**
         * Segédfüggvény convertData függvényhez
         * 
         * @param type $row
         * @param type $key
         */
        protected function convertRow(&$row, $key) {
            $row = array('nev' => $key, 'netto' => $row);
        }

        /**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
 }